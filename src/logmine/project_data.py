"""Module for extracting project data from Redmine responses."""
from collections import namedtuple
from xml.etree import ElementTree


class Project(namedtuple('Project', ['id', 'name'])):
    """Representation of relevant data from a project."""
    pass


def _project_from_element(element):
    """Extract project data from an element

    Args:
        element (Element): an XML element representing a project.
    """
    ident, name = (element.findall('identifier')[0].text,
                   element.findall('name')[0].text)
    return Project(ident, name)


def projects_from_xml(xml):
    """Get a list of projects from Redmine XML response.

    Args:
        xml (bytes): the XML containing the projects.
    """
    projects = ElementTree.fromstring(xml)
    return [_project_from_element(project) for project in projects]
