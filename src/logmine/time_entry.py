from itertools import chain


def make_parameter(key, value):
    return ('{key}="{value}"'.format(key=key, value=value)
            if value else '')


def make_time_entry(issue, time, activity, date=''):
    parameters = (make_parameter('issue_id', issue),
                  make_parameter('hours', time),
                  make_parameter('spent_on', date),
                  make_parameter('activity_id', activity))
    spaces = (' ' if param else '' for param in parameters)
    text_parameters = ''.join(chain.from_iterable(zip(parameters, spaces)))
    return "<time_entry " + text_parameters + "/>"
