"""Module for connecting to a given Redmine with an API key."""

from urllib.request import Request, urlopen
from urllib.parse import urljoin

from logmine.issue_data import issues_from_xml
from logmine.project_data import projects_from_xml
from logmine.activity_data import activities_from_xml


class RedmineSession:
    """A user session on a Redmine server."""
    def __init__(self, redmine_url, api_key):
        """Build a session with a Redmine server.

        Args:
            redmine_url (url): URL pointing to a Redmine server.
            api_key (str): the API key for a given user.
        """
        self.url = redmine_url
        self.key = api_key

    def _build_post_request(self, what, data):
        """Build a POST request to the Redmine server.

        Args:
            what (str): what to request.
            data (bytes): what to post.

        Returns:
            Request: a Request object.

        Raises:
            ValueError: in case the URL is malformed.
        """
        try:
            return Request(urljoin(self.url, what),
                           data=data,
                           headers={'Content-Type': 'text/xml',
                                    'X-Redmine-API-Key': self.key})
        except ValueError as e:
            raise ValueError('Malformed URL: ' + self.url) from e

    def _build_get_request(self, what):
        """Build a GET request to the Redmine server.

        Args:
            what (str): what to request.

        Returns:
            Request: a Request object.

        Raises:
            ValueError: in case the URL is malformed.
        """
        try:
            return Request(urljoin(self.url, what),
                           headers={'X-Redmine-API-Key': self.key})
        except ValueError as e:
            raise ValueError('Malformed URL: ' + self.url) from e

    def _xml_from_server(self, what):
        """Fetch XML from server.

        Args:
            what (str): what to request.

        Returns:
            str: the XML sent by the server.
        """
        request = self._build_get_request(what)
        response = urlopen(request)
        return response.read()

    def log(self, time_entry):
        """Log a time entry to redmine.

        Args:
            time_entry (xml): An xml-encoding time entry for redmine.
        """
        xml_data = bytes(time_entry, encoding='utf-8')
        request = self._build_post_request('time_entries.xml', xml_data)
        urlopen(request)

    def issues(self):
        """Get the issues assigned to the user.

        Returns:
            list: the issues assigned to the user.
        """
        data = self._xml_from_server('issues.xml?assigned_to_id=me')

        return issues_from_xml(data)

    def projects(self):
        """Get the projects visible to the user.

        Returns:
            list: the projects the user can see.
        """
        data = self._xml_from_server('projects.xml')

        return projects_from_xml(data)

    def activities(self):
        """Get the activities that might be used on time entries.

        Returns:
            list: the activities that can be used.
        """
        data = self._xml_from_server('enumerations/time_entry_activities.xml')

        return activities_from_xml(data)
