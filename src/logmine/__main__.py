import sys

from argparse import ArgumentParser

from logmine.commands import attach_all
from logmine.configs import config
from logmine.version import VERSION


def main(arguments):
    arg_parser = ArgumentParser(description="Log time to redmine.")
    arg_parser.add_argument('--version', action='store_true')

    attach_all(arg_parser)

    try:
        config()
    except FileNotFoundError:
        try:
            if arguments[0] != 'config':
                print('No config file found. Generate one with the config '
                      'module.',
                      file=sys.stderr)
                exit(1)
        except IndexError:
            pass

    args = arg_parser.parse_args(arguments)

    if args.version:
        print(__package__, VERSION)
        sys.exit(0)

    try:
        args.function(args)
    except AttributeError:
        arg_parser.print_usage()
        exit(1)


def run():
    main(sys.argv[1:])


if __name__ == '__main__':
    run()
