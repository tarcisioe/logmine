from configparser import ConfigParser
from functools import lru_cache
from os.path import exists, expanduser


CONFPATH = expanduser('~/.logmine')


def new_config():
    c = ConfigParser()
    c.add_section('default')
    c['default']['url'] = ''
    c['default']['key'] = ''
    _write_config(c)


def _write_config(config):
    with open(CONFPATH, 'w') as config_file:
        config.write(config_file)


@lru_cache()
def config():
    c = ConfigParser()
    if exists(CONFPATH):
        c.read(CONFPATH)
    else:
        raise FileNotFoundError('No .logmine found.')

    return c


def write_config():
    c = config()
    _write_config(c)


def change_url(url):
    c = config()
    c['default']['url'] = url
    _write_config(c)


def change_key(key):
    c = config()
    c['default']['key'] = key
    _write_config(c)
