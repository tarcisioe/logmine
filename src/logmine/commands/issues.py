from .session_command import session_command


def attach(subparsers):
    argparser = subparsers.add_parser('issues',
                                      description='Fetch issues assigned to'
                                                  ' the user.')

    return argparser


@session_command(attach)
def issues(_, session):
    for issue in session.issues():
        print('{}: {}'.format(issue.id, issue.subject))
