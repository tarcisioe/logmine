commands = {}


def command(attacher):
    def inner(f):
        commands[f] = attacher
        return f
    return inner


def attach_all(arg_parser, title="subcommands"):
    subparsers = arg_parser.add_subparsers(title=title)

    for function, attacher in commands.items():
        parser = attacher(subparsers)
        parser.set_defaults(function=function)
