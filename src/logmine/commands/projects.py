from .session_command import session_command


def attach(subparsers):
    argparser = subparsers.add_parser('projects',
                                      description='Fetch projects visible to'
                                                  ' the user.')

    return argparser


@session_command(attach)
def projects(_, session):
    for project in session.projects():
        print('{}: {}'.format(project.id, project.name))
