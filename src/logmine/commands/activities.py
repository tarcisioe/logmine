from .session_command import session_command


def attach(subparsers):
    argparser = subparsers.add_parser('activities',
                                      description='Fetch activities usable'
                                                  ' for time entries.')

    return argparser


@session_command(attach)
def activities(_, session):
    for activity in session.activities():
        print('{}: {}'.format(activity.id, activity.name))
