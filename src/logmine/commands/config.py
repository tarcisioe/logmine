from logmine.configs import change_url, change_key, new_config

from .commands import command


def attach(subparsers):
    arg_parser = subparsers.add_parser('config',
                                       description="Configure logmine.")
    arg_parser.add_argument('-u', '--url',
                            action='store',
                            help='change the url pointing to Redmine',
                            default='')
    arg_parser.add_argument('-k', '--key',
                            action='store',
                            help='change the Redmine API key',
                            default='')
    arg_parser.add_argument('-n', '--new', '--reset',
                            action='store_true',
                            help='create a new config file',
                            default='')

    return arg_parser


@command(attach)
def config(args):
    if args.new:
        new_config()

    try:
        if args.url:
            change_url(args.url)

        if args.key:
            change_key(args.key)
    except FileNotFoundError:
        print("No config file found. To create one, use the --new switch.")
