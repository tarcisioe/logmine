import sys

from functools import wraps
from urllib.error import HTTPError, URLError

from logmine.configs import config
from logmine.redmine import RedmineSession

from .commands import command


def _session_wrapper(f):
    @wraps(f)
    def inner(args):
        configs = config()

        url = configs['default']['url']
        key = configs['default']['key']

        session = RedmineSession(url, key)

        try:
            f(args, session)
        except HTTPError as e:
            code = e.getcode()
            if code == 401:
                print('Unauthorized access. Please check your key.',
                      file=sys.stderr)
            elif code == 404:
                print('Request received not found (404). Check your url or the'
                      ' data passed as input.')
            sys.exit(-1)
        except URLError as e:
            print('The URL to Redmine is probably incorrectly set.'
                  ' Please check your configs.',
                  file=sys.stderr)
            sys.exit(-1)
        except ValueError as e:
            print(e, file=sys.stderr)
            sys.exit(-1)

    return inner


def session_command(attacher):
    def inner(f):
        command(attacher)(_session_wrapper(f))
        return f
    return inner
