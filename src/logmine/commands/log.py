from logmine.time_entry import make_time_entry

from .session_command import session_command


def attach(subparsers):
    arg_parser = subparsers.add_parser('log',
                                       description="Make one log entry.")
    arg_parser.add_argument('issue',
                            metavar='ISSUE',
                            help="the issue's id")
    arg_parser.add_argument('time',
                            metavar='TIME',
                            help='how much time was spent.')
    arg_parser.add_argument('-d', '--date',
                            action='store',
                            help='the date the time was spent on, as'
                                 ' YYYY-MM-DD')
    group = arg_parser.add_mutually_exclusive_group()
    group.add_argument('-a', '--activity',
                       action='store',
                       help='the activity name.')
    group.add_argument('-A', '--activity-id',
                       action='store',
                       help='the activity type. (This is a numeric, specific'
                            ' value for every kind of issue. For now, you'
                            ' will have to check your Redmine for possible'
                            ' values.)')

    return arg_parser


@session_command(attach)
def log(args, session):
    if args.activity:
        activities = session.activities()
        filtered = [activity_id
                    for activity_id, name in activities
                    if name == args.activity]

        if not filtered:
            raise ValueError('Activity name is invalid.')

        activity_id, *remainder = filtered

        if remainder:
            raise ValueError('Ambiguous activity name.'
                             ' Please use activity id.')
    else:
        activity_id = args.activity_id

    entry = make_time_entry(args.issue, args.time, activity_id, args.date)

    session.log(entry)
