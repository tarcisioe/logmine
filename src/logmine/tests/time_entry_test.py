import unittest

from logmine.time_entry import make_time_entry


class TimeEntryTest(unittest.TestCase):
    def test_simple_time_entry(self):
        expected = ('<time_entry issue_id="42" hours="7" '
                    'activity_id="9" />')
        entry = make_time_entry(issue="42", time="7")
        self.assertEqual(expected, entry)

    def test_time_entry_with_date(self):
        expected = ('<time_entry issue_id="42" hours="7" '
                    'spent_on="1964-12-13" activity_id="9" />')
        entry = make_time_entry(issue="42", time="7", date="1964-12-13")
        self.assertEqual(expected, entry)

    def test_time_entry_with_activity(self):
        expected = ('<time_entry issue_id="42" hours="7" '
                    'activity_id="26" />')
        entry = make_time_entry(issue="42", time="7", activity='analyze')
        self.assertEqual(expected, entry)

    def test_complete_time_entry(self):
        expected = ('<time_entry issue_id="42" hours="7" '
                    'spent_on="1964-12-13" activity_id="32" />')
        entry = make_time_entry(issue="42", time="7",
                                date="1964-12-13", activity='plan')
        self.assertEqual(expected, entry)
