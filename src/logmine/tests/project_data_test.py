import unittest

from logmine.project_data import projects_from_xml, Project


XML = """<?xml version="1.0" encoding="UTF-8"?>
      <projects type="array">
      <project>
        <name>Project Number 1</name>
        <identifier>proj1</identifier>
      </project>
      <project>
        <identifier>itisppl</identifier>
        <name>Soylent Green</name>
      </project>
      </projects>
      """


class ProjectDataTest(unittest.TestCase):
    """Tests for the project_data module."""
    def test_projects_from_xml(self):
        """Extracts correctly project data from xml."""
        self.assertEqual(projects_from_xml(XML),
                         [Project('proj1', 'Project Number 1'),
                          Project('itisppl', 'Soylent Green')])
