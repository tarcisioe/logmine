"""Tests for the issue_data module."""
import unittest

from logmine.issue_data import issues_from_xml, Issue

XML = """<?xml version="1.0" encoding="UTF-8"?>
    <issues total_count="1" offset="0" limit="25" type="array">
        <issue>
            <id>131264</id>
            <subject>Do stuff for making things</subject>
        </issue>
        <issue>
            <id>41065</id>
            <subject>Do thing for stuff-making</subject>
        </issue>
    </issues>
    """


class IssueDataTest(unittest.TestCase):
    """Tests for the issue_data module."""
    def test_issues_from_xml(self):
        """Extracts correctly issue data from xml."""
        self.assertEqual(issues_from_xml(XML),
                         [Issue('131264', 'Do stuff for making things'),
                          Issue('41065', 'Do thing for stuff-making')])
