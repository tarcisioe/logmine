"""Module for extracting time entry activity data from Redmine responses."""
from collections import namedtuple
from xml.etree import ElementTree


class TimeActivity(namedtuple('TimeActivity', ['id', 'name'])):
    """Representation of relevant data from an issue."""
    pass


def _activity_from_element(element):
    """Extract issue data from an element

    Args:
        element (Element): an XML element representing an issue.

    Returns:
        TimeActivity: the activity that was read.
    """
    ident, name = (element.findall('id')[0].text,
                   element.findall('name')[0].text)
    return TimeActivity(ident, name)


def activities_from_xml(xml):
    """Get a list of time entry activities from Redmine XML response.

    Args:
        xml (bytes): the XML containing the issues.

    Returns:
        list: A list of all TimeActivities extracted.
    """
    activities = ElementTree.fromstring(xml)
    return [_activity_from_element(activity) for activity in activities]
