"""Module for extracting issue data from Redmine responses."""
from collections import namedtuple
from xml.etree import ElementTree


class Issue(namedtuple('Issue', ['id', 'subject'])):
    """Representation of relevant data from an issue."""
    pass


def _issue_from_element(element):
    """Extract issue data from an element

    Args:
        element (Element): an XML element representing an issue.
    """
    ident, subject = (element.findall('id')[0].text,
                      element.findall('subject')[0].text)
    return Issue(ident, subject)


def issues_from_xml(xml):
    """Get a list of issues from Redmine XML response.

    Args:
        xml (bytes): the XML containing the issues.
    """
    issues = ElementTree.fromstring(xml)
    return [_issue_from_element(issue) for issue in issues]
