logmine
=======

A Redmine time logger application/library.

You will need Python 3. Python 3.4+ recommended.

To install, get it through `pip`.
```bash
$ pip install git+https://gitlab.com/tarcisioe/logmine.git#egg=logmine
```

On Windows, git may leave some read-only files that will cause pip
to fail on cleanup. The package will be installed. If you have
any problems with that, you can download this repository as a ZIP
file and install it through pip.

```bash
$ pip install logmine.zip
```

Configuring
-----------

To configure it, use the `config` module, like:

```bash
$ logmine config --new --url https://url/to/redmine/ --key YOUR-KEY-TO-REDMINE-API
```

Your API key can be found under `/my/account` on redmine.

Logging
-------

To log a single entry, use the `log` module:

```bash
$ logmine log TICKET_ID TIME [-d DATE] [-a ACTIVITY] [-A ACTIVITY_ID]
```

Arguments inside `[]` are optional.

`TIME` accepts either an integer, decimal number, or the format `HH:MM`.
`DATE` must be formatted as `YYYY-MM-DD`.
`ACTIVITY` is a name that can be obtained through the activities module.
`ACTIVITY_ID` is an ID that can be obtained through the activities module.
`-a` and `-A` are mutually exclusive.

Fetching issues
---------------

To fetch the issues currently assigned to you, run:

```bash
$ logmine issues
```

Fetching projects
-----------------

To fetch projects visible by you, run:
```bash
$ logmine projects
```

Fetching activities
-------------------

To fetch activities that can be used on time entries, run:

```bash
$ logmine activities
```
