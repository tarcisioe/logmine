from setuptools import setup, find_packages

# The following lines are only needed in the master branch.
import re

VERSION = re.findall(".* = '(.*)'",
                     open('src/logmine/version.py').read())[0]

setup(
    name='logmine',
    version=VERSION,
    packages=find_packages('src'),
    package_dir={'': 'src'},
    entry_points={
        'console_scripts' : [
            'logmine = logmine.__main__:run',
        ],
    },
)
